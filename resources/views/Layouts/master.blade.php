<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
  </head>
  <body>
    <nav class="nav">
      <a class="nav-link active" href="#">Active</a>
      <a class="nav-link" href="#">Link</a>
      <a class="nav-link" href="#">Another link</a>
      <a class="nav-link disabled" href="#">Disabled</a>
    </nav>
    @yield('content')
    <foother>
      <p>
        &copy;laravel & Belajar Koding 2016
      </p>
    </foother>
  </body>
</html>
