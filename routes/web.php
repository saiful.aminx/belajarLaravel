<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Member', 'UseController@index');
Route::get('/Member/{id}', 'UseController@show');
Route::get('/User', 'UseController@user');
