<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UseController extends Controller
{
    public function index()
    {
      return view('Home/test');
    }
    public function show($id)
    {
      $dataArray = ['satu', 'dua', 'tiga'];
      return view('Home/blog', ['id' => $id, 'data' => $dataArray]);
    }
    public function User()
    {
      $users = DB::table('users')->get();
      return view('Home/test', ['users' => $users]);
    }
}
